import React from 'react';
import ReactDOM from 'react-dom';
import Config from '../../config.js'
import SimpleMap from '../ui/map.js'
import { Link } from 'react-router-dom';

const customStyles = {
  content : {
  	width                 : '80%',
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)',
    maxHeight             : "70vh",
	maxWidth              : "90vw",
	overflow              : "scroll",
  }
};



class CharityInstance extends React.Component
{
	constructor(props)
	{
		super(props);
		try
		{
			this.id = this.props.match.params.id
			this.endpoint = Config.host + Config.endpoints.charities + "?id="+this.id
		} catch
		{
			this.id = null;
			this.endpoint = null;
		}
		
		this.state = {
			name: null,
			address: null,
			city: null,
			state: null,
			category: null,
			latitude: null,
			longitude: null,
			zipCode: null,
			nearByRestaurants : [],
			income: null
		}
	}


	// componentWillReceiveProps(nextProps)
	// {
	// 	console.log('http://api.charitylink2.me/charity?zipcode='+nextProps.zipCode)
	// 	fetch('http://api.charitylink2.me/charity?zipcode='+nextProps.zipCode,
	// 		{
	// 			  method:'GET',
	// 		      credentials: 'include',
	// 		      headers: {
	// 		          'Accept': 'application/json',
	// 		          'Content-Type': 'application/json',
	// 		      }
	// 		}).then(response => response.json())
	// 	    .then(json => 
	// 	    {
	// 	    	this.state.nearByRestaurants = []
	// 	    	for(var i in json)
	// 	    	{
	// 	    		this.state.nearByRestaurants.push(json[i].name);
	// 	    	}
	// 	    	this.setState(this.state);
	// 	    }).catch(e =>
	// 	    {
	// 	        console.log("http://api.charitylink2.me/charity?zipcode="+nextProps.zipCode)
	// 	   });

	// }
	componentDidMount()
	{

		fetch(this.endpoint,
			{
				  method:'GET',
			      credentials: 'include',
			      headers: {
			          'Accept': 'application/json',
			          'Content-Type': 'application/json',
			      }
			}).then(response => response.json())
		    .then(json => 
		    {
				if(json.length > 0)
				{
					this.state = {...this.state, ...json[0]}
					this.setState(this.state)
					fetch(Config.host + Config.endpoints.restaurants + "?zipcode="+json[0].zipCode,
						{
							  method:'GET',
						      credentials: 'include',
						      headers: {
						          'Accept': 'application/json',
						          'Content-Type': 'application/json',
						      }
						}).then(response => response.json())
					    .then(json => 
					    {
							this.state.nearByRestaurants = json;
							this.setState(this.state);
					    }).catch(e =>
					    {
					        console.log(this.endpoint);
					    });
					fetch(Config.host + Config.endpoints.income + "?zip="+json[0].zipCode,
						{
							  method:'GET',
						      credentials: 'include',
						      headers: {
						          'Accept': 'application/json',
						          'Content-Type': 'application/json',
						      }
						}).then(response => response.json())
					    .then(json => 
					    {
					    	console.log(json)
							this.state.income = json[0]["AMOUNT"];
							this.setState(this.state);
					    }).catch(e =>
					    {
					        console.log(this.endpoint);
					});
				}
		    }).catch(e =>
		    {
		        console.log(this.endpoint);
		    });


	}

	visitZipcode(zipcode)
	{
		console.log(zipcode);
		this.props.handleClick(Config.host + Config.endpoints.income + "?zip="+zipcode)
	}
	//pull relational data etc.

	render()
	{
		let map = null
		map =  this.state.latitude != null ? <SimpleMap height="30vh" latitude={this.state.latitude} longitude={this.state.longitude} name="test"/> : null;
		let nearByRestaurants_link = []
		for(let i in this.state.nearByRestaurants)
		{
			nearByRestaurants_link.push(<li key={nearByRestaurants_link.length}><Link to={"/restinfo/"+this.state.nearByRestaurants[i].id} key={nearByRestaurants_link.length}>{this.state.nearByRestaurants[i].name}</Link></li>)
		}
		return (
			<div className="jumbotron">
			  <div className="row">
			  	  <div className="col-lg-8">
			  	  	  <div className="card">
				          <img className='card-img-top' style={{objectFit: 'cover', maxHeight: '40vh', width:'100%'}} src="" onError={(e)=>{e.target.src="https://www.scandichotels.se/Static/img/placeholders/image-placeholder_3x2.svg"}}/>
				          <div className="card-body">
				          		<h4 >{this.state.name}</h4>

							    <ul>
							         <li>City: {this.state.city}</li>
							         <li>State: {this.state.state}</li>
							         <li>Zipcode: <Link to={"/zipinfo/"+this.state.zipCode}>{this.state.zipCode}</Link></li>
							         <li>Website: {this.state.website}</li>
							         <li>Donation URL: {this.state.donationURL}</li>
							         <li>Accepting Donation: {this.props.acceptingDonations == 1 ? "Yes" : "No"}</li>
				        			 <li>Average income of this area: <b>${this.state.income/100} / Month</b> </li>
							         <li>Mission Statement : <p> {this.props.missionStatement} </p> </li>
							        
				      			</ul>
			      		  </div>
			      	  </div>
		      	   </div>
		      	   <div className="col-md-4">
		      	   		<div className="card mb-3">
		      	   			<div className="card-header">
		      	   				<h4> Map </h4>
		      	   			</div>
		      	   			<div className="card-body">
			      	   			<div style={{"align" : "center"}}>
									{map}
				  			  	</div>	
			  			  	</div>
			  			</div>
		      	   		<div className="card mb-3">
		      	   			<div className="card-header">
		      	   				<h4> Nearby Restaurants </h4>
		      	   			</div>
		      	   			<div className="card-body" style={{maxHeight:"30vh", overflow:"scroll"}}>
		      	   				<ul>
		      	   					{nearByRestaurants_link}
		      	   				</ul>
		      	   			</div>
		      	   		</div>
		      	   </div>
	      	   </div>
	        </div>
	     )
	}
}

export default CharityInstance;