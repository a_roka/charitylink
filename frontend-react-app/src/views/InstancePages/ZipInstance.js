import React from 'react';
import Config from '../../config.js'
import SimpleMap from '../ui/map.js'
import { Link } from 'react-router-dom';
import utils from '../../utils/utils.js';

class ZipInstance extends React.Component
{
	constructor(props)
	{
		super(props);
		this.id = this.props.match.params.id
		this.endpoint = Config.host + Config.endpoints.income + "?zip="+this.id
		this.state = {
			zipCode: this.props.match.params.id,
			nearByCharities : [],
			nearByRestaurants : [],
			income: null
		}
	}

	componentDidMount()
	{

		fetch(this.endpoint,
			{
				  method:'GET',
			      credentials: 'include',
			      headers: {
			          'Accept': 'application/json',
			          'Content-Type': 'application/json',
			      }
			}).then(response => response.json())
		    .then(json => 
		    {
				if(json.length > 0)
				{
					this.state = {...this.state, ...json[0]}
					this.setState(this.state)
				}
		    }).catch(e =>
		    {
		        console.log(this.endpoint);
		    });
		fetch(Config.host + Config.endpoints.charities + "?zipcode="+this.state.zipCode,
						{
							  method:'GET',
						      credentials: 'include',
						      headers: {
						          'Accept': 'application/json',
						          'Content-Type': 'application/json',
						      }
						}).then(response => response.json())
					    .then(json => 
					    {
							this.state.nearByCharities = json;
							this.setState(this.state);
					    }).catch(e =>
					    {
					        console.log(this.endpoint);
		});

		fetch(Config.host + Config.endpoints.restaurants + "?zipcode="+this.state.zipCode,
						{
							  method:'GET',
						      credentials: 'include',
						      headers: {
						          'Accept': 'application/json',
						          'Content-Type': 'application/json',
						      }
						}).then(response => response.json())
					    .then(json => 
					    {
							this.state.nearByRestaurants = json;
							this.setState(this.state);
					    }).catch(e =>
					    {
					        console.log(this.endpoint);
		});

	}
	//pull relational data etc.

	render()
	{
		let map = null
		map =  this.state.latitude != null ? <SimpleMap height="30vh" latitude={this.state.latitude} longitude={this.state.longitude} name="test"/> : null;
		let nearByCharities_link = []
		for(let i in this.state.nearByCharities)
		{
			nearByCharities_link.push(<li key={"char"+nearByCharities_link.length}><Link to={"/charityinfo/"+this.state.nearByCharities[i].id} >{this.state.nearByCharities[i].name}</Link></li>)
		}
		let nearByRestaurants_link = []
		for(let i in this.state.nearByRestaurants)
		{
			nearByRestaurants_link.push(<li key={"rest"+nearByRestaurants_link.length}><Link to={"/restinfo/"+this.state.nearByRestaurants[i].id}>{this.state.nearByRestaurants[i].name}</Link></li>)
		}
		return (
			<div className="jumbotron">
			  <div className="row">
			  	  <div className="col-lg-8">
			  	  	  <div className="card">
				          <SimpleMap height="40vh" latitude={this.state.latitude} longitude={this.state.longitude} name="test"/> 
				          <div className="card-body">
				          		<h4>{utils.pad(this.state.zipCode)}</h4>

							    <ul>
							         <li>Average income of this area: <b>${this.state.AMOUNT/100} / Month</b> </li>
							        
				      			</ul>
			      		  </div>
			      	  </div>
		      	   </div>
		      	   <div className="col-md-4">
		      	   		<div className="card mb-3">
		      	   			<div className="card-header">
		      	   				<h4> Nearby Restaurants</h4>
		      	   			</div>
		      	   			<div className="card-body" style={{maxHeight:"30vh", overflow:"scroll"}}>
			      	   			<div style={{"align" : "center"}}>
									{nearByRestaurants_link}
				  			  	</div>	
			  			  	</div>
			  			</div>
		      	   		<div className="card mb-3">
		      	   			<div className="card-header">
		      	   				<h4> Nearby Charities </h4>
		      	   			</div>
		      	   			<div className="card-body" style={{maxHeight:"30vh", overflow:"scroll"}}>
		      	   				<ul>
		      	   					{nearByCharities_link}
		      	   				</ul>
		      	   			</div>
		      	   		</div>
		      	   </div>
	      	   </div>
	        </div>
	     )
	}
}

export default ZipInstance;