import React from "react";
import Config from "../../config.js";

import static_data from "../../static_data.js";
import "../../css/d3.css";
import * as d3 from "d3";

function drawMap(id, data, toolTip) {
  function mouseOver(d) {
    d3.select("#tooltip")
      .transition()
      .duration(200)
      .style("opacity", 0.9);

    d3.select("#tooltip").html(toolTip(d.n, data[d.id]));
  }

  function mouseOut() {
    d3.select("#tooltip")
      .transition()
      .duration(500)
      .style("opacity", 0);
  }
  console.log(data);
  d3.select(id)
    .selectAll(".state")
    .data(static_data.uStatePaths)
    .enter()
    .append("path")
    .attr("class", "state")
    .attr("d", function(d) {
      return d.d;
    })
    .style("fill", function(d) {
      if (d.id in data) return data[d.id].color;
      else return "#ffffcc";
    })
    .on("click", mouseOver);
}

function pad(num, size) {
  var s = num + "";
  while (s.length < size) s = "0" + s;
  return s;
}
function tooltipHtml(n, d) {
  /* function to create html content string in tooltip div. */
  const ordered = {};
  Object.keys(d.cities)
    .sort()
    .forEach(function(key) {
      ordered[key] = d.cities[key];
    });
  d.cities = ordered;
  var html = "<h4>" + n + "</h4><table>";
  for (let city in d.cities) {
    html +=
      "<tr><td>" + city + "</td><td>" + d.cities[city].value + "</td></tr>";
  }
  html += "</table>";
  return html;
}

class D3Map extends React.Component {
  constructor(props) {
    super(props);
    this.endpoint = Config.host + Config.endpoints[this.props.type];
    this.state = {
      loading: false
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
    if (this.props.type == "chstats") {
      var sampleData = static_data.statePov;

      for (let state in sampleData) {
        sampleData[state].color = d3.interpolate("#ffffcc", "#800026")(
          sampleData[state].count / 20
        );
      }
      drawMap("#statesvg".concat(this.props.type), sampleData, tooltipHtml);
      this.setState({ loading: false });
    } else {
      fetch(this.endpoint, {
        method: "GET",
        credentials: "include",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        }
      })
        .then(response => response.json())
        .then(json => {
          var sampleData = {}; /* Sample random data. */

          for (let state in static_data.states) {
            sampleData[state] = {
              cities: {},
              count: 0,
              color: null
            };
          }

          var max = 0;
          if (
            this.props.type == "charities" ||
            this.props.type == "restaurants"
          ) {
            for (let i in json) {
              let instance = json[i];
              if (
                !(
                  instance.city.toUpperCase() in
                  sampleData[instance.state].cities
                )
              ) {
                sampleData[instance.state].cities[
                  instance.city.toUpperCase()
                ] = {
                  value: 0,
                  count: 0
                };
              }
              sampleData[instance.state].count += 1;
              sampleData[instance.state].cities[
                instance.city.toUpperCase()
              ].value += 1;
              sampleData[instance.state].cities[
                instance.city.toUpperCase()
              ].count += 1;
              if (sampleData[instance.state].count > max)
                max = sampleData[instance.state].count;
            }
          } else if (this.props.type == "income") {
            for (let i in json) {
              let instance = json[i];
              let city = static_data.zip_state[pad(instance.ZIP, 5)].city;
              let state = static_data.zip_state[pad(instance.ZIP, 5)].state;
              if (!(city.toUpperCase() in sampleData[state].cities)) {
                sampleData[state].cities[city.toUpperCase()] = {
                  value: 0,
                  count: 0
                };
              }
              sampleData[state].count += 1;
              sampleData[state].cities[city.toUpperCase()].value +=
                instance.AMOUNT;
              sampleData[state].cities[city.toUpperCase()].count += 1;
              if (sampleData[state].count > max) max = sampleData[state].count;
            }
          }

          for (let state in sampleData) {
            sampleData[state].color = d3.interpolate("#ffffcc", "#800026")(
              sampleData[state].count / max
            );
            if (this.props.type == "income") {
              for (let city in sampleData[state].cities)
                sampleData[state].cities[city].value =
                  "$" +
                  Math.round(
                    sampleData[state].cities[city].value /
                      sampleData[state].cities[city].count /
                      100
                  ).toString() +
                  " / Month";
            }
          }

          drawMap("#statesvg".concat(this.props.type), sampleData, tooltipHtml);
          this.setState({ loading: false });
        });
    }
  }

  render() {
    return (
      <div className="jumbotron" id={"mapd3".concat(this.props.type)}>
        <h3>
          
          {this.props.type.charAt(0).toUpperCase() +
            this.props.type.slice(1)}
          Stats Map
        </h3>
        <img
          style={{
            display: this.state.loading ? "block" : "none",
            width: "30vw"
          }}
          src="http://css-workshop.com/wp-content/themes/mocca/img/loader.gif"
          alt="loading"
        />
        <div className="row">
          <div className="col-md-8">
            <svg
              preserveAspectRatio="xMinYMin meet"
              viewBox="0 0 1280 800"
              id={"statesvg".concat(this.props.type)}
            />
          </div>
          <div className="col-md-3">
            <div
              id="tooltip"
              style={{ maxHeight: "40vh", overflow: "scroll" }}
            />
          </div>
        </div>
      </div>
    );
  }
}
export default D3Map;
