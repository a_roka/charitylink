import expect from 'expect';
import D3Map from './D3Map.js';
import React from 'react';
import { shallow } from 'enzyme';

describe("D3Map.js", () => {
  test("Renders without exploding", () => {
  	// AUTHOR: David
  	var wrapper = shallow(<D3Map type="restaurants" />);
    expect(wrapper.length).toBe(1);
    // AUTHOR: David
    wrapper = shallow(<D3Map type="charities" />);
    expect(wrapper.length).toBe(1);
    // AUTHOR: David
    wrapper = shallow(<D3Map type="income" />);
    expect(wrapper.length).toBe(1);
  });

});