import React from 'react';
import SearchBar from './SearchBar.js';
import expect from 'expect';
import { shallow } from 'enzyme';

describe("Tests SearchBar.js", () => {

    test("Renders without exploding", () => {
      // AUTHOR: David
      const wrapper = shallow(<SearchBar />);
  
      expect(wrapper.length).toBe(1);
      expect(wrapper).toBeDefined();
      
    });
  
  });
