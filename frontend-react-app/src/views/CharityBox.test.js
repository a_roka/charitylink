import React from 'react';
import CBox from './CharityBox';
import expect from 'expect';
import { shallow } from 'enzyme';

// AUTHOR: David
describe("Tests CharityBox.js", () => {

    test("Renders without exploding", () => {

      const wrapper = shallow(<CBox />);
  
      expect(wrapper.length).toBe(1);
      expect(wrapper).toBeDefined();
      
    });
  
  });
