import React from 'react';
import RestBox from './RestBox.js'
import CharityBox from './CharityBox.js'
import IncomeBox from './IncomeBox.js'
import Config from '../config.js'

class SearchBar extends React.Component {
	constructor(props)
	{
		super(props);
		this.endpoint = Config.host + "/search";
		try
		{
			this.query = this.props.match.params.query
			if(this.query != "" && typeof this.query != "undefined")
				this.endpoint += "?query="+this.query
		} catch {}
		this.categories = {
				"restaurants" : 
				{
					toRender: [],
					box: RestBox
				},
				"charities" :
				{
					toRender: [],
					box: CharityBox

				},
				"income" :
				{
					toRender: [],
					box: IncomeBox
				}
		}
		this.state = {
			model: "restaurants",
			loading: false,
			page : 0
		}
		this.getData = this.getData.bind(this)
		this.relationAction = this.relationAction.bind(this);
		this.changeModel = this.changeModel.bind(this)
		this.changePage = this.changePage.bind(this)

	}

	componentDidMount()
	{
		this.setState({loading : true});
		this.getData((data) => {
			this.setState({loading : false})
		})
	}

	relationAction(id)
	{
		if(this.state.model == "restaurants")
		{
			this.props.handleClick("/restinfo/"+id)
		} else if(this.state.model == "charities"){
			this.props.handleClick("/charityinfo/"+id)
		} else if(this.state.model == "income"){
			this.props.handleClick("/zipinfo/"+id)
		}
	}

	getData(callback){
		fetch(this.endpoint,
		{
				method:'GET',
				credentials: 'include',
			    headers: {
				    'Accept': 'application/json',
				    'Content-Type': 'application/json'
				}
		}).then(response => response.json())
	    .then(json => {
	    	for(let key in this.categories)
	    	{
	    		let data =  json[key] //data for this category
	    		for(let i in data)
	    		{
	    			var BoxComponent = this.categories[key].box
	    			this.categories[key].toRender.push(<BoxComponent key={key+data[i].id} relation={this.relationAction} query={this.query} {...data[i]}/>)
	    		} 
	    	}
	    	callback(json);
	    }).catch(e =>
	    {
	    	callback(null);
	        console.log("Failed to fetch")
	    })
	}

	

	changePage(event)
	{
		this.setState({page : parseInt(event.target.value)});
	}

	changeModel(model)
	{
		this.setState({model : model})
	}

	render()
	{
		let toRender = this.categories[this.state.model].toRender
		let nav = []
		for(let category in this.categories)
		{
			nav.push(<li key={nav.length} className={"nav-item " + (this.state.model == category ? "active" : "")} onClick={() => this.changeModel(category)}><a className={"nav-link " + (this.state.model == category ? "active" : "")}>{category.charAt(0).toUpperCase() + category.slice(1)}</a></li>)
		}
		let pagination = []
		if(toRender != null)
		{
			for(var i = 0; i < toRender.length; i+=12)
			{
				pagination.push(<option key={pagination.length} value={i/12}>{i/12+1}</option>);
			}
		}
		return (
		    	<div className='jumbotron'>
		    		<div className="mb-2">
		    			<h3>Searching for "{this.query}"</h3>
			    		<ul className="nav nav-tabs">
			    			{nav}
			    		</ul>
						<div style={{marginLeft:"35vw"}}>
				    			<img style={{display: this.state.loading ? "block" : "none", width: "30vw"}} src="http://css-workshop.com/wp-content/themes/mocca/img/loader.gif" alt="loading"/>
				    	</div>
				    	<div className="mb-2">
			    			<a style={{display: this.state.loading ? "none": "inline-block"}} className="btn btn-primary btn-sm" onClick={() => {this.setState({loading : true}); this.getData((data) => this.setState({loading : false}))}} role="button">Reload</a>
				    	</div>		    		
				    	<div className='row'>
							{toRender ? toRender.slice(this.state.page * 12, (this.state.page+1) * 12) : null}
						</div>
						<div className="paginate wrapper">
						        <select value={this.state.page} onChange={(this.changePage)} >
						            {pagination}
						        </select>
					    </div>
				    </div>
				</div>
	    )
	}
}

export default SearchBar;
