from flask import Flask, jsonify, json, Response, request
import os, requests
from flask_cors import CORS, cross_origin
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from config import Config
from sqlalchemy.orm import sessionmaker


app = Flask(__name__)
cors = CORS(app)
app.config['DEBUG'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://ar0ka:f$lA{8jK*p@charlinkdbid.cbevuo9srcf8.us-east-2.rds.amazonaws.com/charlinkdb'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['CORS_HEADERS'] = 'Content-Type'
gitlabParams = {'PRIVATE-TOKEN': "FdZoCaXfXo_HQmKs2vJz"}
yelpHeader = {'Authorization': "Bearer YELh2_bAS8DFaXUPBrPSNZYm02mRsYUV2ewPr7xgW6_lH1kBTZXv1QhSWC1epC1lQafE-99mmKNjqrxOZesMitGfGe5XqM5LKSBEQkZe3_Xe4uIlj6SY74QE66yuW3Yx"}

##############
# SqlAlchemy #
##############
db = SQLAlchemy(app)
Session = sessionmaker(bind=db.engine)

ma = Marshmallow(app)
class INCOME(db.Model):
	ZIP = db.Column(db.Integer, primary_key=True)
	AMOUNT = db.Column(db.Integer)

class RESTAURANT(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(50))
	category = db.Column(db.String(30))
	address = db.Column(db.String(40))
	city = db.Column(db.String(15))
	zipCode = db.Column(db.Integer)
	state = db.Column(db.String(5))
	latitude = db.Column(db.Float)
	longitude = db.Column(db.Float)
	imageURL = db.Column(db.String(100))
	phone = db.Column(db.String(20))
	price = db.Column(db.String(5))
	rating = db.Column(db.Float)
	combined = db.Column(db.String(500))

class CHARITY(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100))
	city = db.Column(db.String(15))
	zipCode = db.Column(db.Integer)
	state = db.Column(db.String(5))
	latitude = db.Column(db.Float)
	longitude = db.Column(db.Float)
	category = db.Column(db.String(80))
	website = db.Column(db.String(100))
	missionStatement = db.Column(db.String(200))
	acceptingDonations = db.Column(db.Integer)
	donationURL = db.Column(db.String(100))
	combined = db.Column(db.String(500))


class incomeSchema(ma.Schema):
    class Meta:
        fields = ('ZIP',
        	'AMOUNT')

class restaurantSchema(ma.Schema):
    class Meta:
        fields = ('id',
        	'name',
        	'category',
        	'address',
        	'city',
        	'zipCode',
        	'state',
        	'latitude',
        	'longitude',
        	'imageURL',
        	'phone',
        	'price',
        	'rating',
			'combined')

class charitySchema(ma.Schema):
    class Meta:
        fields = ('id',
        	'name',
        	'city',
        	'zipCode',
        	'state',
        	'latitude',
        	'longitude',
        	'category',
        	'website',
        	'missionStatement',
        	'acceptingDonations',
        	'donationURL',
			'combined')

class charityAndIncomeSchema(ma.Schema):
    class Meta:
        fields = ('id',
        	'name',
        	'city',
        	'zipCode',
        	'state',
        	'latitude',
        	'longitude',
        	'category',
        	'website',
        	'missionStatement',
        	'acceptingDonations',
        	'donationURL',
			'combined',
			'ZIP',
			'AMOUNT')

incomes_schema = incomeSchema(many=True)
income_schema = incomeSchema()
restaurants_schema = restaurantSchema(many=True)
restaurant_schema = restaurantSchema()
charitys_schema = charitySchema(many=True)
charity_schema = charitySchema()
charities_income_schema = charityAndIncomeSchema(many=True)


'''
Filtering Functions
'''
def filterLowerboundIncome(target, baseQuery, TABLE):
	'''
	target: target income
	baseQuery: BaseQuery object (filter stacking)
	TABLE: targeting table/collection
	'''
	target = int(target)
	zipcode_list = [item.ZIP for item in INCOME.query.filter(INCOME.AMOUNT >= target).all()]
	return baseQuery.filter(TABLE.zipCode.in_(zipcode_list))

def filterUpperboundIncome(target, baseQuery, TABLE):
	'''
	target: target income
	baseQuery: BaseQuery object (filter stacking)
	TABLE: targeting table/collection
	'''
	target = int(target)
	zipcode_list = [item.ZIP for item in INCOME.query.filter(INCOME.AMOUNT <= target).all()]
	return baseQuery.filter(TABLE.zipCode.in_(zipcode_list))


def filterLowerboundIncomeTable(target, baseQuery, TABLE): # specifically for income table
	'''
	target: target income
	baseQuery: BaseQuery object (filter stacking)
	TABLE: targeting table/collection
	'''
	target = int(target)
	return baseQuery.filter(TABLE.AMOUNT >= target)

def filterUpperboundIncomeTable(target, baseQuery, TABLE): # specifically for income table
	'''
	target: target income
	baseQuery: BaseQuery object (filter stacking)
	TABLE: targeting table/collection
	'''
	target = int(target)
	return baseQuery.filter(TABLE.AMOUNT <= target)

SEARCH_FIELDS = {
	"id"   : lambda query, baseQuery, TABLE: baseQuery.filter_by(id = int(query)).limit(1),
	"query": lambda query, baseQuery, TABLE: baseQuery.filter(TABLE.combined.like("%"+query+"%")), # search query
	"zipcode":  lambda query, baseQuery, TABLE: baseQuery.filter_by(zipCode=query),
	"zip":  lambda query, baseQuery, TABLE: baseQuery.filter_by(ZIP=query),
	"lower": filterLowerboundIncome,
	"upper": filterUpperboundIncome,
	"slower": filterLowerboundIncomeTable,
	"supper": filterUpperboundIncomeTable,
	"state": lambda query, baseQuery, TABLE: baseQuery.filter_by(state=query),
	"city": lambda query, baseQuery, TABLE: baseQuery.filter_by(city=query),
	"foodType": lambda query, baseQuery, TABLE: baseQuery.filter_by(category=query),
	"state": lambda query, baseQuery, TABLE: baseQuery.filter_by(state=query)
}


######################
#Gitlab parsing stuff#
######################
def handleCommitsCount(output):
	commits_count = 0
	try:
		page = 1
		while True:
			all_commits = requests.get('https://gitlab.com/api/v4/projects/8572644/repository/commits?all=true&per_page=100&page='+str(page), params=gitlabParams)
			json_data = all_commits.json();
			for commit in json_data:
				commit_owner = findMemberByAuthorName(commit["author_name"])
				if commit_owner != None:
					output["members"][commit_owner]["commits"] += 1
					commits_count += 1
			if all_commits.headers['X-Page'] == all_commits.headers['X-Total-Pages']: # more to fetch
				break
			page += 1
	except Exception:
		return 0
	return commits_count

def handleTestsCount(output):
	total_tests = 0
	for member in output["members"]:
		first_name = member.split(" ")[0]
		output["members"][member]["tests"] = getNumTests(first_name)
		total_tests +=  output["members"][member]["tests"]
	return total_tests



def handleIssuesCount(output):
	issues_count = 0
	try:
		page = 1
		while True:
			all_issues = requests.get('https://gitlab.com/api/v4/projects/8572644/issues?all=true&per_page=100&page='+str(page), params=gitlabParams)
			json_data = all_issues.json();
			for issue in json_data:
				issue_owner = findMemberByAuthorID(issue["author"]["id"])
				if issue_owner != None:
					output["members"][issue_owner]["issues"] += 1
					issues_count += 1
			if all_issues.headers['X-Page'] == all_issues.headers['X-Total-Pages']: # more to fetch
				break
			page += 1
	except Exception:
		return 0
	return issues_count

def findMemberByAuthorName(author_name):
	for member in Config.GITLAB:
		if author_name in Config.GITLAB[member]["author_name"]:
			return member
	return None

def findMemberByAuthorID(author_id):
	for member in Config.GITLAB:
		if author_id in Config.GITLAB[member]["author_id"]:
			return member
	return None

def getNumTests(user):
	c = 0
	for file_path in Config.TESTS_URLS:
		try:
			with open(file_path) as f:
				r = f.read()
				c += r.count(user)
		except Exception:
			continue
	return c

##########
# GITLAB #
##########

@app.route('/gitlab')
@cross_origin()
def total():
	result = {"members" : {}, "total_issues": 0, "total_commits": 0, "total_tests": 0}
	# all_commits = requests.get('https://gitlab.com/api/v4/projects/8572644/repository/commits?all=true&per_page=100', params=gitlabParams)
	# all_issues = requests.get('https://gitlab.com/api/v4/projects/8572644/issues?all=true&per_page=100', params=gitlabParams)
	for member in Config.GITLAB:
		result["members"][member] = {
			"commits" : 0,
			"issues": 0,
			"tests": 0
		}
	result["total_issues"] = handleIssuesCount(result)
	result["total_commits"] = handleCommitsCount(result);
	result["total_tests"] = handleTestsCount(result);
	return jsonify(result)

@app.route('/gitlab/issues')
@cross_origin()
def totalIssues():
	r = requests.get('https://gitlab.com/api/v4/projects/8572644/issues', params=gitlabParams)
	return jsonify(count = len(json.loads(r.text)))

@app.route('/gitlab/issues/<id>')
@cross_origin()
def issuesByUserID(id):
	r = requests.get('https://gitlab.com/api/v4/projects/8572644/issues?author_id='+id, params=gitlabParams)
	return jsonify(count = len(json.loads(r.text)))

@app.route('/gitlab/commits')
@cross_origin()
def totalCommits():
	string = '"author_name"'
	c = 0
	r = requests.get('https://gitlab.com/api/v4/projects/8572644/repository/commits?all=true&per_page=100', params=gitlabParams)

	page = 2
	while(r.text != "[]"):
		c += r.text.count(string)
		r = requests.get('https://gitlab.com/api/v4/projects/8572644/repository/commits?all=true&per_page=100&page='+str(page), params=gitlabParams)
		page += 1

	return jsonify(count = c)

@app.route('/gitlab/commits/<user>')
@cross_origin()
def commitsByUserName(user):
	if(user=="aitorct"):
		user = "Aitor C"
	if(user=="hysed97"):
		user = "Hassan Syed"
	if(user=="Alsritt"):
		user = "Alex Strittmatter"

	r = requests.get('https://gitlab.com/api/v4/projects/8572644/repository/commits?all=true&per_page=100', params=gitlabParams)
	string = '"author_name":"'+user+'"'
	return jsonify(count = r.text.count(string))

@app.route('/gitlab/tests')
@cross_origin()
def totalTests():
	string = "AUTHOR:"
	c = 0
	for file_path in Config.TESTS_URLS:
		try:
			with open(file_path) as f:
				r = f.read()
				c += r.count(string)
		except Exception:
			continue

	return jsonify(count = c)

@app.route('/gitlab/tests/<user>')
@cross_origin()
def testsByUserName(user):
	return jsonify(count = getNumTests(user))



########
# Utils #
########
'''
@app.route('/yelp/nearby/<zipcode>')
@cross_origin()
def nearbyRestaurants(zipcode):
 	coords = getLatitudeLongitude(zipcode)
 	r = requests.get('https://api.yelp.com/v3/businesses/search?latitude='+coords[0]+'&longitude='+coords[1], headers=yelpHeader)
 	return Response(r.text, mimetype='application/json')


def getLatitudeLongitude(zipcode):
	r = requests.get('https://api.promaptools.com/service/us/zip-lat-lng/get/?zip='+zipcode+'&key=17o8dysaCDrgv1c')
	data = r.json()
	latitude = data["output"][0]["latitude"]
	longitude = data["output"][0]["longitude"]
	return [latitude, longitude]


@app.route('/yelp/restaurants/<id>')
@cross_origin()
def getRestaurantInfo(id):
	r = requests.get('https://api.yelp.com/v3/businesses/'+id, headers=yelpHeader)
	return Response(r.text, mimetype='application/json')
'''

'''
Loading coords data
'''

def getLatitudeLongitudeAPI(zipcode):
	r = requests.get('https://api.promaptools.com/service/us/zip-lat-lng/get/?zip='+zipcode+'&key=17o8dysaCDrgv1c')
	data = r.json()
	latitude = data["output"][0]["latitude"]
	longitude = data["output"][0]["longitude"]
	print(latitude, longitude)
	return float(latitude), float(longitude)
zipcode_coords = { }

def loadCoordsData(zipcode_coords):
	with open(Config.COORDS_PATH) as f:
		f = open(Config.COORDS_PATH)
		lines = f.readlines()
		for i in range(1, len(lines)):
			line = lines[i]
			data = line.split(",")
			zipcode_coords[data[0]] = {
				"lat": float(data[1]),
				"long": float(data[2])
			}


loadCoordsData(zipcode_coords)

def getLatitudeLongitude(zipcode):
	try:
		return zipcode_coords[zipcode]["lat"], zipcode_coords[zipcode]["long"]
	except Exception:
		try:
			lat,longt = getLatitudeLongitudeAPI(zipcode_str) #lazy cache the coords
			zipcode_coords[zipcode_str ] = {
				"lat": lat,
				"long": longt
			}
			with open(Config.COORDS_PATH, "a") as myfile:
				myfile.write("\n"+zipcode_str+","+str(lat)+","+str(longt))
		except Exception:
			return 0, 0



############
# DATABASE #
############
@app.route('/income', methods=['GET'])
@cross_origin()
def get_income_zip():
	incomeList = INCOME.query;
	for arg in request.args:
		if arg == 'slower' or arg == 'supper' or arg == 'zip':
			incomeList = SEARCH_FIELDS[arg](request.args.get(arg), incomeList, INCOME)

	data = incomes_schema.jsonify(incomeList).json
	if(len(data) > 0):
		for item in data:
			item["latitude"], item["longitude"] = getLatitudeLongitude(str(item["ZIP"]).zfill(5))
	elif "zip" in request.args: #requesting for zipcode but we dont have this data
		#still return coords
		zipcode = int(request.args["zip"]);
		zipcode_str = str(zipcode).zfill(5)
		lat, longt = getLatitudeLongitude(str(zipcode_str ).zfill(5))

		data = [{
			"AMOUNT": -1,
			"ZIP":  int(request.args["zip"]),
			"latitude": lat,
			"longitude": longt
		}]
	return jsonify(data)

@app.route('/restaurant', methods=['GET'])
@cross_origin()
def get_restaurant():
	restaurantList = RESTAURANT.query;
	for arg in request.args:
		if arg in SEARCH_FIELDS:
			restaurantList = SEARCH_FIELDS[arg](request.args.get(arg), restaurantList, RESTAURANT)
	return restaurants_schema.jsonify(restaurantList)



@app.route('/charity', methods=['GET'])
@cross_origin()
def get_charity():
	charityList = CHARITY.query;
	for arg in request.args:
		if arg in SEARCH_FIELDS:
			charityList = SEARCH_FIELDS[arg](request.args.get(arg), charityList, CHARITY)
	return charitys_schema.jsonify(charityList)

@app.route('/search', methods=['GET'])
@cross_origin()
def search():
	charityList = CHARITY.query
	restaurantList = RESTAURANT.query
	incomeList = INCOME.query
	for arg in request.args:
		if arg in SEARCH_FIELDS:
			charityList = SEARCH_FIELDS[arg](request.args.get(arg), charityList, CHARITY)
			restaurantList = SEARCH_FIELDS[arg](request.args.get(arg), restaurantList, RESTAURANT)
			if arg == "query":
				incomeList = SEARCH_FIELDS["zip"](request.args.get(arg), incomeList, INCOME)
			if arg == "lower":
				incomeList = SEARCH_FIELDS["slower"](request.args.get(arg), incomeList, INCOME)
			if arg == "upper":
				incomeList = SEARCH_FIELDS["supper"](request.args.get(arg), incomeList, INCOME)

	data = incomes_schema.jsonify(incomeList).json
	if(len(data) > 0):
		for item in data:
			item["latitude"], item["longitude"] = getLatitudeLongitude(str(item["ZIP"]).zfill(5))

	result = {
		"charities": charitys_schema.dump(charityList).data,
		"restaurants": restaurants_schema.dump(restaurantList).data,
		"income": data
	}

	return jsonify(result)

@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Credentials', 'true')
    return response

if __name__ == "__main__":
    app.secret_key = os.urandom(12)
    app.run(debug=True, host='0.0.0.0', port=8080)
